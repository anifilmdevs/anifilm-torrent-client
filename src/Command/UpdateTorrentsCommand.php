<?php

namespace App\Command;

use App\Model\ReleaseModel;
use App\Service\AFDownloader;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Dumper\DumperInterface;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\VarDumper;

class UpdateTorrentsCommand extends Command
{
    /**
     * @var AFDownloader
     */
    private $AFDownloader;

    /**
     * @var ReleaseModel
     */
    private $releaseModel;
    /**
     * @var LoggerInterface
     */
    private $logger;
    private $env;


    public function __construct($name = null, AFDownloader $AFDownloader, ReleaseModel $releaseModel, LoggerInterface $logger, $env)
    {
        parent::__construct($name);
        $this->AFDownloader = $AFDownloader;
        $this->releaseModel = $releaseModel;
        $this->logger = $logger;
        $this->env = $env;
    }

    public static $defaultName = "afts:update";

    protected function configure()
    {
        $this->addOption('force', 'F', InputOption::VALUE_NONE, 'Всё равно перекачать торренты, даже если этого не нужно делать.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Throwable
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(date('Y-m-d H:i:s') . ' Я родился');
        $rssReleases = $this->AFDownloader->getRssReleases();

        $force = $input->getOption('force');

        $output->writeln('Найдено ' . $rssReleases->count() . ' релизов через RSS.');
//        foreach ($rssReleases as $release) {
//            $managedRelease = $this->releaseModel->getManagedVersion($release);
//            foreach ($managedRelease->getTorrents() as $torrent) {
//                dump($torrent->getTransmissionModel()->getSize());
//            }
//        }
//
//        die;

        foreach ($rssReleases as $release) {
            try {
                $managedRelease = $this->releaseModel->getManagedVersion($release);
                if (!$managedRelease) {
                    $managedRelease = $release;
                    $output->writeln('Впервые вижу релиз ' . $release->getId());
                } elseif (($managedRelease->getUpdatedAt()->format('Y-m-d H:i:s') === $release->getUpdatedAt()->format('Y-m-d H:i:s')) && !$force) {
                    $output->writeln('Релиз ' . $release->getId() . ' и так свежий, пропускаю');
                    continue;
                } else {
                    $output->writeln('В релизе ' . $release->getId() . ' что-то обновилось, ща посмотрим. Локальная дата: ' .
                        $managedRelease->getUpdatedAt()->format('Y-m-d H:i:s') . ', Удалённая дата: ' . $release->getUpdatedAt()->format('Y-m-d H:i:s'));
                }

                $this->AFDownloader->setForceReaddEqualTorrents($force);
                $this->AFDownloader->importTorrents($managedRelease);
                $managedRelease->setUpdatedAt($release->getUpdatedAt());
                $this->releaseModel->save($managedRelease);
            } catch (\Throwable $exception) {
                $this->logger->warning('Pizdec: ' . $exception->getMessage());
                if ($this->env === "dev") {
                    throw $exception;
                }
            }
        }
        $output->writeln(date('Y-m-d H:i:s') . ' Я дошёл до конца и выжил');
    }
}