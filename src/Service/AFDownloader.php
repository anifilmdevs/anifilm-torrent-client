<?php

namespace App\Service;

use App\Entity\Release;
use App\Entity\Torrent;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\Comparison;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use PHPHtmlParser\Dom;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Transmission\Transmission;

class AFDownloader
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var string
     */
    private $torrentFileDir;
    /**
     * @var TransmissionManager
     */
    private $transmission;

    private $forceReaddEqualTorrents = true;

    private $suppressExceptions = true;

    /**
     * AFDownloader constructor.
     * @param LoggerInterface $logger
     * @param TransmissionManager $transmission
     * @param string $torrentFileDir
     * @param string $env
     */
    public function __construct(LoggerInterface $logger, TransmissionManager $transmission, $torrentFileDir, $env)
    {
        $this->client = new Client();
        $this->logger = $logger;
        $this->torrentFileDir = $torrentFileDir;
        $this->transmission = $transmission;
        if ($env === "dev") {
            $this->suppressExceptions = false;
        } else {
            $this->suppressExceptions = true;
        }
    }

    /**
     * @return Release[]|ArrayCollection
     * @throws \Exception
     */
    public function getRssReleases()
    {
        $rss = $this->client->get('https://anifilm.tv/rss')->getBody()->getContents();

        $rssObject = simplexml_load_string($rss);

        $releases = new ArrayCollection();

        foreach ($rssObject->channel->item as $item) {
            $releases[] = $release = new Release();
            $release
                ->setId($this->idFromLink($item->link, '/releases/'))
                ->setUpdatedAt(\DateTime::createFromFormat('D, d M Y H:i:s O', $item->pubDate))
            ;
        }

        return $releases;
    }

    /**
     * @param Release $release
     * @throws \Exception
     * @throws GuzzleException
     */
    public function importTorrents($release)
    {
        try {
            $response = $this->client->get('https://anifilm.tv/releases/' . $release->getId());
            $dom = $this->getDomFromResponse($response);
            /** @var Dom\Collection|Dom\HtmlNode[] $torrentDownloadButtons */
            $torrentDownloadButtons = $dom->find('.release__torrents-btn > a.btn--green');
            if (count($torrentDownloadButtons) === 0) {
                $this->logger->warning('У релиза ' . $release->getId() . ' торрентов нема. Добавь что ли, а то ругаться буду.');
            }
            foreach ($torrentDownloadButtons as $button) {
                $torrentId = (int)$this->idFromLink($button->getAttribute('href'), '/releases/download-torrent/');
                $dbTorrentSearch = $release->getTorrents()->matching(
                    Criteria::create()->where(new Comparison('id', Comparison::EQ, $torrentId))
                );
                if ($dbTorrentSearch->count() === 1) {
                    $torrent = $dbTorrentSearch->first();
                } else {
                    $torrent = new Torrent();
                    $torrent
                        ->setRelease($release)
                        ->setId($torrentId)
                    ;
                    $release->getTorrents()->add($torrent);
                }

                foreach ($release->getTorrents() as $torrent) {
                    $this->downloadOrUpdateTorrent($torrent);
                }
            }
        } catch (GuzzleException $exception) {
            $this->logger->warning('Pizdec при получении торрентов для релиза ' . $release->getId() . ': ' . $exception->getMessage());
            if (!$this->suppressExceptions) {
                throw $exception;
            }
        }
    }

    public function getLastReleaseLinks($page = 1)
    {
        $hrefs = [];
        $page = $this->client->get('https://anifilm.tv/releases/page/' . $page);
        $dom = $this->getDomFromResponse($page);

        /**
         * @var Dom\HtmlNode[] $titles
         */
        $titles = $dom->find('a.releases__title-russian');

        foreach ($titles as $title) {
            $hrefs[] = $title->getAttribute('href');
        }

        dump($hrefs);
//        $dom->find();
    }

    /**
     * @param ResponseInterface $response
     * @return Dom
     */
    private function getDomFromResponse(ResponseInterface $response)
    {
        $dom = new Dom();
        return $dom->load($response->getBody()->getContents());
    }

    /**
     * @param null|string $link
     * @param string $replaceFromStart
     * @return string
     * @throws \Exception
     */
    private function idFromLink(?string $link, string $replaceFromStart)
    {
        $linkPath = (new ArrayCollection(parse_url($link)))['path'];
        if (strpos($linkPath, $replaceFromStart) !== 0) {
            $this->logger->warning('Pizdec с разбором ссылки на релиз. Ссылка: ' . $link);
            throw new \Exception();
        }

        return str_replace($replaceFromStart, '', $linkPath);
    }

    /**
     * @param Torrent $torrent
     * @throws \Exception
     * @throws RequestException
     */
    private function downloadOrUpdateTorrent($torrent)
    {
        $response = $this->client->get('https://anifilm.tv/releases/download-torrent/' . $torrent->getId());
        $fileContents = $response->getBody()->getContents();
        $contentDisposition = $response->getHeader('Content-Disposition');
        $newFilename = $torrent->getId() . '.torrent';
        if (count($contentDisposition) > 0) {
            preg_match_all('/filename="(?\'filename\'.*)"/mU', $contentDisposition[0], $filenameMatches);
            if (isset($filenameMatches['filename'][0]) && $filenameMatches['filename'][0]) {
                $newFilename = $filenameMatches['filename'][0];
            }
        }

        $isNew = true;
        if ($torrent->getFile()) {
            $oldFullPath = $this->torrentFileDir . '/' . $torrent->getFile();
            if (file_exists($oldFullPath)) {
                $oldTorrentFile = file_get_contents($oldFullPath);
            } else {
                $this->logger->warning('Файл торрента дял релиза ' . $torrent->getRelease()->getId() . ' куда-то пропал. Точно помню, как клал его вот сюда: ' . $oldFullPath);
                $oldTorrentFile = '';
            }

            if (($oldTorrentFile === $fileContents) && !$this->forceReaddEqualTorrents) {
                $this->logger->warning('Содержимое .torrent файла совпадает с уже сохранённым для релиза ' . $torrent->getRelease()->getId() . '. Ничего не делаю.');
                return;
            }
            $isNew = false;
        }
        $torrent->setFile($newFilename);
        $newFullPath = $this->torrentFileDir . '/' . $torrent->getFile();
        $torrentFile = fopen($newFullPath, 'w');
        fwrite($torrentFile, $fileContents);
        if (!$isNew) {
            $this->transmission->replaceWithNewVersion($torrent);
            $torrent->setUpdatedAt(new \DateTime());
            if ($oldFullPath !== $newFullPath) {
                unlink($oldFullPath);
            }
        } else {
            $this->transmission->addTorrent($torrent);
        }
    }

    /**
     * @param bool $forceReaddEqualTorrents
     */
    public function setForceReaddEqualTorrents(bool $forceReaddEqualTorrents): void
    {
        $this->forceReaddEqualTorrents = $forceReaddEqualTorrents;
    }
}