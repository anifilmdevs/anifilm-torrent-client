<?php

namespace App\Service;

use App\Entity\Torrent;
use App\Object\TorrentModelStub;
use Psr\Log\LoggerInterface;
use Transmission\Client;
use Transmission\Model\Torrent as TorrentModel;
use Transmission\Transmission;

class TransmissionManager
{
    /**
     * @var Transmission
     */
    private $transmission;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $torrentFileDir;


    public function __construct(LoggerInterface $logger, $torrentFileDir, $host = null, $port = null, $path = null, $username = null, $password = null)
    {
        $this->transmission = new Transmission();
        $client = new Client($host, $port, $path);
        if ($username || $password) {
            $client->authenticate($username, $password);
        }
        $this->transmission->setClient($client);
        $this->logger = $logger;
        $this->torrentFileDir = $torrentFileDir;
    }

    /**
     * @param Torrent $torrent
     *
     * @throws \Exception
     */
    public function addTorrent(Torrent $torrent)
    {
        /** @var TorrentModel $torrentModel */
        $torrentModel = $this->transmission->add($this->torrentFileDir . '/' . $torrent->getFile());

        // При добавлении возвращается неполная модель. Нужна прям ваще полная. Поэтому тут этот костыль.
        $torrentModel = $this->getTorrentModel($torrentModel->getId());

        $torrent->setTransmissionId($torrentModel->getId());
        $this->generateTorrentModelStub($torrent, $torrentModel);
    }

    /**
     * @param Torrent $torrent
     * @throws \Exception
     */
    public function replaceWithNewVersion(Torrent $torrent)
    {
        try {
            $this->removeTorrent($torrent);
        } catch (\Exception $exception) {
            $this->logger->warning('Пытался удалить торрент для ' . $torrent->getRelease()->getId() . ', но получил ошибку: ' . $exception->getMessage());
        }
        $this->addTorrent($torrent);
    }

    public function removeTorrent(Torrent $torrent, bool $withFiles = false)
    {
        $model = $torrent->getTransmissionModel();
        if ($model) {
            $this->transmission->remove($torrent->getTransmissionModel(), $withFiles);
        } else {
            $this->logger->warning("Tried to remove unregistered torrent for release " . $torrent->getRelease()->getId());
        }
    }

    public function getTorrentModel($id)
    {
        return $this->transmission->get($id);
    }

    public function generateTorrentModelStub(Torrent $torrent, ?TorrentModel $model = null)
    {
        $torrent->setTransmissionModelStub(new TorrentModelStub($this, $torrent->getTransmissionId(), $model));
    }
}