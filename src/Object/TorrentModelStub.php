<?php

namespace App\Object;

use App\Service\TransmissionManager;
use Transmission\Model\Torrent;

class TorrentModelStub
{
    /**
     * @var TransmissionManager
     */
    private $resolver;

    /**
     * @var Torrent|null
     */
    private $model;

    /**
     * @var int
     */
    private $id;

    /**
     * TorrentModelStub constructor.
     * @param $resolver
     * @param $id
     * @param null $model
     */
    public function __construct($resolver, $id, $model = null)
    {
        $this->resolver = $resolver;
        $this->id = $id;
        $this->model = $model;
    }

    /**
     * @return null|Torrent
     */
    public function getModel()
    {
        if ($this->model) {
            return $this->model;
        }

        $this->model = $this->resolver->getTorrentModel($this->id);
        return $this->model;
    }
}