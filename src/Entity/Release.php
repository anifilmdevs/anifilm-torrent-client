<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Release
 * @package App\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="releases")
 */
class Release
{
    /**
     * @var int|null
     *
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @var ArrayCollection|Torrent[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Torrent", mappedBy="release", cascade={"all"})
     */
    private $torrents;


    public function __construct()
    {
        $this->torrents = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param null|string $id
     * @return Release
     */
    public function setId(?string $id): Release
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime|null $updatedAt
     * @return Release
     */
    public function setUpdatedAt(?\DateTime $updatedAt): Release
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return Torrent[]|ArrayCollection
     */
    public function getTorrents()
    {
        return $this->torrents;
    }

    /**
     * @param Torrent[]|ArrayCollection $torrents
     * @return Release
     */
    public function setTorrents($torrents)
    {
        $this->torrents = $torrents;
        return $this;
    }
}