<?php

namespace App\Entity;

use App\Object\TorrentModelStub;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Torrent
 * @package App\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="torrents")
 */
class Torrent
{
    /**
     * @var int|null
     *
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Release|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Release", inversedBy="torrents")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $release;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    private $file;

    /**
     * @var int|null
     *
     * @ORM\Column(name="transmission_id", type="integer", nullable=true)
     */
    private $transmissionId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="first_added_at", type="datetime")
     */
    private $firstAddedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var TorrentModelStub|null
     */
    private $transmissionModelStub;


    public function __construct()
    {
        $this->firstAddedAt = new \DateTime();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Torrent
     */
    public function setId(?int $id): Torrent
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Release|null
     */
    public function getRelease(): ?Release
    {
        return $this->release;
    }

    /**
     * @param Release|null $release
     * @return Torrent
     */
    public function setRelease(?Release $release): Torrent
    {
        $this->release = $release;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getFile(): ?string
    {
        return $this->file;
    }

    /**
     * @param null|string $file
     * @return Torrent
     */
    public function setFile(?string $file): Torrent
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return int
     */
    public function getTransmissionId(): ?int
    {
        return $this->transmissionId;
    }

    /**
     * @param int $transmissionId
     * @return Torrent
     */
    public function setTransmissionId(?int $transmissionId): Torrent
    {
        $this->transmissionId = $transmissionId;
        return $this;
    }

    /**
     * @return null|\Transmission\Model\Torrent
     */
    public function getTransmissionModel(): ?\Transmission\Model\Torrent
    {
        return $this->transmissionModelStub->getModel();
    }

    /**
     * @param TorrentModelStub $stub
     * @return Torrent
     */
    public function setTransmissionModelStub(TorrentModelStub $stub): Torrent
    {
        $this->transmissionModelStub = $stub;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFirstAddedAt(): \DateTime
    {
        return $this->firstAddedAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime|null $updatedAt
     * @return Torrent
     */
    public function setUpdatedAt(?\DateTime $updatedAt): Torrent
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}