<?php

namespace App\Model;

use App\Entity\Release;
use App\Entity\Torrent;
use Doctrine\ORM\EntityManagerInterface;

class ReleaseModel
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Release $release
     * @return null|Release|object
     */
    public function getManagedVersion(Release $release)
    {
        $repo = $this->em->getRepository(Release::class);

        return $repo->find($release->getId());
    }

    public function save($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

}