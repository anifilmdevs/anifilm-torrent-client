<?php

namespace App\EventListener;

use App\Entity\Torrent;
use App\Service\TransmissionManager;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class TorrentListener
{
    /**
     * @var TransmissionManager
     */
    private $transmission;

    public function __construct(TransmissionManager $transmission)
    {
        $this->transmission = $transmission;
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Torrent) {
            return;
        }

        $this->transmission->generateTorrentModelStub($entity);
    }
}